import React from 'react';
import { Card, Input, Button, Row, Col, Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

const QuizAdditionalMaterials = (props) => {
    return (
        <Card title="Dodatkowe materiały do quizu">
            Opis quizu (podstawowa pomoc)
            <Input.TextArea 
                rows={4} 
                value={props.data.basicHelp}
                onChange={e => props.onChange('basicHelp', e.target.value)}
            />
            <Upload>
                <Button icon={<UploadOutlined />}>Dodaj dodatkowe pliki z materiałami</Button>
            </Upload>
        </Card>
    )
}

export default QuizAdditionalMaterials;