import React from 'react';
import { Modal, Input } from 'antd';

const CategoryModal = (props) => {
    return (
        <Modal
            visible={props.visible}
            title='Dodawanie kategorii'
            okText='Dodaj kategorię'
            cancelText='Anuluj'
            onCancel={props.onCancel}
            onOk={props.onFinish}
        >
            Nazwa:
            <Input 
                value={props.categoryName} 
                onChange={(e) => props.onChangeCategoryName(e.target.value)}
            />
        </Modal>
    );
}

export default CategoryModal;