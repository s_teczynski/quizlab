import React from 'react';
import { Row, Col, Modal, Input, Checkbox, InputNumber, Button, Upload, message } from 'antd';
import { MinusCircleOutlined, PlusCircleOutlined, LoadingOutlined, PlusOutlined } from '@ant-design/icons';

const QuestionModal = (props) => {
    const uploadButton = (
        <div>
            <PlusOutlined />
            <div style={{ marginTop: 8 }}>Dodaj pytanie jako obrazek</div>
        </div>
    );

    return (
        <Modal
            visible={props.visible}
            width={1000}
            onCancel={props.closeModal}
            onOk={() => props.onFinish(props.question)}
        >
            <Row>
                Treść pytania:
                <Input.TextArea 
                    rows={8}
                    value={props.question.content} 
                    onChange={e => props.onChange('content', e.target.value)}
                />
            </Row>
            <Row>
                <Upload
                    name="avatar"
                    listType="picture-card"
                    className="avatar-uploader"
                    showUploadList={false}
                    beforeUpload={props.beforeUpload}
                >
                    {props.question.imgUrl && !props.uploadingImage && <img src={props.question.imgUrl} alt="avatar" style={{ width: '100%' }} />}
                    {!props.question.imgUrl && !props.uploadingImage && uploadButton}
                    {props.uploadingImage && <LoadingOutlined />}
                </Upload>
            </Row>
            <Row>
                <Checkbox 
                    checked={props.question.predefinedAnswers}
                    onChange={e => props.onChange('predefinedAnswers', e.target.checked)}
                >
                    Czy chcesz dodać predefiniowane odpowiedzi?
                </Checkbox>
            </Row>

            {props.question.predefinedAnswers && props.question.answers.map((answer, index) => (
                    <Row style={{marginTop: 10}} gutter={8}>
                        <Col span={16}>
                            <Input 
                                value={answer.content} 
                                onChange={(e) => props.onAnswerChange('content', index, e.target.value)} 
                            />
                        </Col>
                        <Col span={4}>
                            <Checkbox 
                                checked={answer.isCorrect}
                                onChange={(e) => props.onAnswerChange('isCorrect', index, e.target.checked)} 
                            >
                                    Poprawna?
                            </Checkbox>
                        </Col>
                        <Col span={4}>
                            <Button 
                                type="link" 
                                icon={<MinusCircleOutlined />}
                                onClick={() => props.deleteAnswer(index)}
                            >
                                Usuń odpowiedź
                            </Button>
                        </Col>
                    </Row>
            ))}
            {props.question.predefinedAnswers && 
                <Button
                    type="link"
                    icon={<PlusCircleOutlined />}
                    onClick={() => props.addAnswer()}
                >
                    Dodaj odpowiedź
                </Button>
            }

            <Row>
                Liczba punktów
            </Row>
            <Row>
                <InputNumber 
                    value={props.question.points} 
                    min={1}
                    onChange={e => props.onChange('points', e)}
                />
            </Row>
            <Row>
                Czas na odpowiedź(s)
            </Row>
            <Row>
                <InputNumber 
                    value={props.question.timeForAnswer} 
                    min={1}
                    onChange={e => props.onChange('timeForAnswer', e)}
                />
            </Row>
            <Row>
                Wyjaśnienie odpowiedzi:
                <Input.TextArea 
                    value={props.question.answerExplanation} 
                    onChange={e => props.onChange('answerExplanation', e.target.value)}
                />
            </Row>

        </Modal>
    );
}

export default QuestionModal;