import React, { Component } from 'react';
import { Card, Table, Button } from 'antd';

import QuestionModal from './QuestionModal';

const QuestionsArea = (props) => {
    const columns = [
        {
            title: 'Pytanie',
            dataIndex: 'content',
            key: 'content',
        },
        {
            title: 'Ilość odpowiedzi',
            key: 'answersAmount',
            dataIndex: 'answers',
            render: (answers) => answers.length
        },
        {
            title: 'Punkty',
            dataIndex: 'points',
            key: 'points',
        },
        {
            title: 'Akcje',
            dataIndex: 'actions',
            key: 'actions',
            render: (_, row, index) => (
                <Button 
                    type="link" 
                    style={{padding: 0}} 
                    onClick={() => { props.showModal(); props.changeQuestionMode('edit', index) }}
                >
                    Edytuj
                </Button>
            )
        }
    ];

    return (
        <Card
            title="Pytania"
            extra={<Button type="primary" onClick={() => { props.showModal(); props.changeQuestionMode('add') }}>Dodaj nowe pytanie</Button>}
        >
            <Table dataSource={props.questions} columns={columns} />
            <QuestionModal
                visible={props.modalVisible}
                question={props.question}
                closeModal={props.closeModal}
                onChange={props.onInputChange}
                onAnswerChange={props.onAnswerChange}
                deleteAnswer={props.onDeleteAnswer}
                addAnswer={props.onAddAnswer}
                onFinish={props.onFinishModal}
                beforeUpload={props.beforeUpload}
                uploadingImage={props.uploadingImage}
            />
        </Card>
    );
}

export default QuestionsArea;