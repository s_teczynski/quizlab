import React from 'react';
import { Card, Input, Checkbox, Row, Col, Button, Select } from 'antd';
const { Option } = Select;


const GeneralQuizInfo = (props) => {
    return (
        <Card title="Podstawowe informacje">
            <Row>
                <Col span={8}>
                    Nazwa:
                    <Input 
                        value={props.basicInformation.name}
                        onChange={e => props.onChange('name', e.target.value)} 
                    />
                </Col>
            </Row>
            <Row>
                <Col span={8}>
                    <Row align='middle'>
                        <Col>
                            Kategoria &nbsp;
                        </Col>
                        <Col>
                            <Button type="link" onClick={props.onClickCategoryButton} style={{padding: 0}}>
                                (Dodaj kategorię)
                            </Button>
                        </Col>
                    </Row>
                    <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Wybierz kategorię"
                        optionFilterProp="children"
                        value={props.basicInformation.category}
                        onChange={e => props.onChange('category', e)}
                        disabled={props.doesQuizExist}
                    >
                        {props.categories.map(category => (
                            <Option value={category.id}>
                                {category.name} ({category.quizesAmount})
                            </Option>
                        ))}
                    </Select>
                </Col>
                <Col>
                    
                </Col>
            </Row>
            <Row>
                <Checkbox
                    checked={props.basicInformation.isPublic}
                    onChange={e => props.onChange('isPublic', e.target.checked)} 
                >
                    Publiczny?
                </Checkbox>
            </Row>
            <Row>
                <Checkbox
                    value={props.basicInformation.isPublished}
                    onChange={e => props.onChange('isPublished', e.target.checked)} 
                >
                    Opublikowany?
                </Checkbox>
            </Row>
        </Card>
    )
}

export default GeneralQuizInfo;