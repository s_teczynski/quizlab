import axios from 'axios';

const mode = 'development';
const baseURL = mode === 'development' ? 'http://localhost:3001' : 'http://quizlab-env.eba-ndjpfx7j.us-east-2.elasticbeanstalk.com';

const api = axios.create({
    baseURL: baseURL,
});

api.interceptors.request.use(function (config) {
    const token = localStorage.getItem('token');
    config.headers.Authorization = token ? `Bearer ${token}` : '';

    return config;
});

api.interceptors.response.use(
    function (response) {
        return response;
    },
    function (error) {
        return Promise.reject(error);
    }
)

export { api };