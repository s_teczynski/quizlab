import React from 'react';
import { withRouter, Switch, Route, RouteComponentProps, Link, Redirect } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import { Button, Layout } from 'antd';

import QuizesView from './views/QuizesView';
import AddOrEditQuiz from './views/AddOrEditQuiz';
import LoginView from './views/LoginView';

import { api } from './api';

import './App.css';
import 'antd/dist/antd.css';

const { Header, Footer, Sider, Content } = Layout;

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            user: null,
            loadingUser: true
        }
    }

    componentDidMount() {
        const token = localStorage.getItem('token');

        this.setState({
            user: token ? jwt_decode(token) : null,
            loadingUser: false
        });
    }
    

    render() {
        const { user, loadingUser } = this.state;
                
        return (
            <Layout>
                {!user && !loadingUser && !this.isAnonymousAccessPath() && <Redirect to="/login" />}
                {user && 
                    <Header style={{background: '#ABA6A6'}}>
                        {user.email},
                        <Button type='link' onClick={this.handleLogout}>Wyloguj się</Button>
                    </Header>
                }
                
                <Content style={{width: '90%', margin: 'auto', paddingTop: 20}}>
                    <Switch>
                        <Route path="/login" render={() => <LoginView handleLogin={this.handleLogin} />} />
                        <Route path="/quizes" component={QuizesView} />
                        <Route path="/quiz/new" component={AddOrEditQuiz} />
                        <Route path="/quiz/:id" component={AddOrEditQuiz} />
                    </Switch>
                </Content>

            </Layout>
        );
    }

    isAnonymousAccessPath = () => {
        const anonymousLocations = [
            '/login'
        ];

        const subLocation = window.location.hash.slice(1);
        return anonymousLocations.indexOf(subLocation) >= 0;
    }

    handleLogin = (data) => {        
        api
            .post('/login', data)
            .then(res => res.data)
            .then(token => {localStorage.setItem('token', token); return token;})
            .then(token => this.setState({ user: jwt_decode(token) }))
            .then(() => this.props.history.push('/quizes'))
            .catch(err => console.log(err));
    }

    handleLogout = () => {
        localStorage.clear();
        this.setState({
            user: null
        });
    }

}

export default withRouter(App);
