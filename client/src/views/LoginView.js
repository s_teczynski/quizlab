import React, { Component } from 'react'
import { Row, Card, Form, Input, Button } from 'antd';

export default class LoginView extends Component {
    render() {
        return (
            <Row align='middle' justify='center' style={{ height: '100vh' }}>
                <div style={{background: 'white', padding: 150}}>
                    <Form
                        name="basic"
                        initialValues={{ remember: true }}
                        onFinish={this.props.handleLogin}
                    >
                        <Form.Item
                            name="email"
                            rules={[{ required: true, message: 'Please input your username!' }]}
                        >
                            <Input placeholder='Email' />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: 'Please input your password!' }]}
                        >
                            <Input.Password placeholder='Hasło' />
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                Zaloguj
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </Row>
        )
    }
}
