import React from 'react';
import { Card, Table, Button } from 'antd';
import { api } from '../api';

export default class QuizesView extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            quizes: []
        }
    }
    
    componentDidMount() {
        this.fetchQuizes();
    }
    
    render() {
        return (
            <>
                <Card 
                    title="Twoje quizy" 
                    extra={<Button type="primary" onClick={() => this.props.history.push('quiz/new')}>Dodaj nowy quiz</Button>}
                >
                    <Table dataSource={this.state.quizes} columns={this.columns} />
                </Card>
            </>
        );
    }

    fetchQuizes = () => {
        api
            .get('/quizes/mine')
            .then(res => res.data)
            .then(quizes => this.setState({ quizes: quizes }))
    }

    columns = [
        {
            title: 'Nazwa',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Kategoria',
            dataIndex: 'category',
            key: 'category',
        },
        {
            title: 'Ilość pytań',
            dataIndex: 'questionsAmount',
            key: 'questionsAmount',
        },
        {
            title: 'Akcje',
            dataIndex: 'actions',
            key: 'actions',
            render: (_, row) => (
                <Button 
                    type="link" 
                    style={{padding: 0}} 
                    onClick={() => this.props.history.push(`/quiz/${row.id}`)}
                >
                    Edytuj
                </Button>
            )
        }
    ];
}