import React from 'react';
import { Button, Space, Row, message } from 'antd';
import { api } from '../api';
import _ from 'lodash';

import GeneralQuizInfo from '../components/GeneralQuizInfo';
import QuizAdditionalMaterials from '../components/QuizAdditionalMaterials';
import QuestionsArea from '../components/QuestionsArea';
import CategoryModal from '../components/CategoryModal';

export default class AddOrEditQuiz extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            modalVisible: false,
            questions: [],
            categories: [],
            question: JSON.parse(JSON.stringify(this.emptyQuestion)),
            basicInformation: this.initialBasicInformation,
            additionalMaterials: this.initialAdditionalMaterials,
            questionMode: 'add',
            editedQuestionIndex: -1,
            newCategoryName: '',
            categoryModalVisible: false,
            uploadingImage: false
        }
    }
    
    componentDidMount() {
        const { id } = this.props.match.params;
        this.fetchCategories();
        if (id) {
            this.fetchQuizInfo();
        }
    }
    

    render() {
        const { 
            questions, 
            basicInformation, 
            additionalMaterials, 
            modalVisible, 
            question, 
            newCategoryName, 
            categoryModalVisible,
            categories,
            uploadingImage
        } = this.state;

        return (
            <>
                <Space direction='vertical'>
                    <GeneralQuizInfo 
                        basicInformation={basicInformation}
                        onChange={this.onBasicInformationChange}
                        onClickCategoryButton={this.showCategoryModal}
                        categories={categories}
                        onChangeCategory={(categoryId) => this.setState({ selectedCategory: categoryId})}
                        doesQuizExist={Boolean(this.props.match.params.id)}
                    />
                    <CategoryModal
                        visible={categoryModalVisible}
                        categoryName={newCategoryName}
                        onChangeCategoryName={(name) => this.setState({ newCategoryName: name })}
                        onFinish={this.handleAddCategory}
                        onCancel={this.closeCategoryModal}
                    />
                    <QuizAdditionalMaterials 
                        data={additionalMaterials}
                        onChange={this.onAdditionalMaterialsChange}
                    />
                    <QuestionsArea 
                        modalVisible={modalVisible}
                        questions={questions}
                        question={question}
                        onInputChange={this.onQuestionChange}
                        onAnswerChange={this.onAnswerChange}
                        onAddAnswer={this.onAddAnswer}
                        onDeleteAnswer={this.onDeleteAnswer}
                        closeModal={this.closeModal}
                        showModal={this.showModal}
                        onFinishModal={this.onFinishModal}
                        changeQuestionMode={this.changeQuestionMode}
                        beforeUpload={this.beforeUpload}
                        uploadingImage={uploadingImage}
                    />
                    <Row justify='center'>
                        <Button 
                            type="primary" 
                            onClick={this.onFinish}
                        >
                            Zapisz
                        </Button>
                    </Row>
                </Space>
            </>
        );
    }

    fetchQuizInfo = () => {
        const { id } = this.props.match.params;
        api
            .get(`/quizes/${id}`)
            .then(res => res.data)
            .then(data => this.unpackQuizInformation(data));

    }

    unpackQuizInformation = (data) => {
        this.setState({
            basicInformation: {
                name: data.quiz.name,
                category: data.quiz.categoryId,
                isPublic: data.quiz.isPublic,
                isPublished: data.quiz.isPublished
            },
            additionalMaterials: {
                basicHelp: data.quiz.additionalHelp,
                files: []
            },
            questions: data.questions.map(question => {
                return ({
                    id: question.id,
                    content: question.questionText,
                    predefinedAnswers: !question.plainInput,
                    answers: question.answers,
                    timeForAnswer: question.timeForAnswer,
                    answerExplanation: question.answerExplanation,
                    points: question.points,
                    imgUrl: question.questionPhoto
                })
            })
        })
    }

    fetchCategories = () => {
        api
            .get('/quizCategories')
            .then(res => res.data)
            .then(categories => this.setState({ categories: categories }));
    }

    onFinish = async () => {
        const { basicInformation, additionalMaterials, questions } = this.state;
        
        const { id } = this.props.match.params;

        const body = {
            basicInformation, 
            additionalMaterials, 
            questions
        };
        
        if (id) {
            await api
                .patch(`/quizes/${id}`, body)
        } else {
            await api
                .post('/quizes', body);
        }

        this.props.history.push('/quizes');

    }

    onBasicInformationChange = (param, value) => {
        this.setState(prevState => ({
            basicInformation: {
                ...prevState.basicInformation,
                [param]: value
            }
        }))
    }

    onAdditionalMaterialsChange = (param, value) => {
        this.setState(prevState => ({
            additionalMaterials: {
                ...prevState.additionalMaterials,
                [param]: value
            }
        }))
    }

    onFinishModal = (question) => {
        if (this.state.questionMode === 'edit') {
            const { editedQuestionIndex } = this.state;
            this.setState(prevState => ({
                questions: prevState.questions.map((mapQuestion, mapIndex) => {
                    if (editedQuestionIndex === mapIndex) {
                        mapQuestion = question;
                    }
                    return mapQuestion;
                })
            }), () => this.closeModal())
        }
    
        if (this.state.questionMode === 'add') {
            this.setState(prevState => ({
                questions: [...prevState.questions, question]
            }), () => this.closeModal())
        }
    }

    onQuestionChange = (param, value) => {
        this.setState(prevState => ({
            question: {
                ...prevState.question,
                [param]: value
            }
        }))
    }

    onAnswerChange = (param, index, newValue) => {
        const answersCopy = this.state.question.answers;
        if (param === 'isCorrect') {
            answersCopy.forEach(answer => {
                answer.isCorrect = false;
            })
        }
        answersCopy[index][param] = newValue;
        
        this.setState(prevState => ({
            question: {
                ...prevState.question,
                answers: answersCopy
            }
        }));
    }

    onAddAnswer = () => {
        const newAnswer = {content: 'Tutaj wpisz treść odpowiedzi', isCorrect: false};
        this.setState(prevState => ({
            question: {
                ...prevState.question,
                answers: [...prevState.question.answers, newAnswer]
            }
        }))
    }

    onDeleteAnswer = (indexToDelete) => {
        this.setState(prevState => ({
            question: {
                ...prevState.question,
                answers: prevState.question.answers.filter((_, index) => index !== indexToDelete)
            }
        }))
    }

    closeModal = () => {
        this.setState({
            modalVisible: false
        })
    }

    showModal = () => {
        this.setState({
            modalVisible: true
        })
    }

    changeQuestionMode = (newMode, editedIndex = -1) => {
        if (editedIndex >= 0) {
            this.setState(prevState => ({
                questionMode: newMode,
                editedQuestionIndex: editedIndex,
                question: prevState.questions[editedIndex]
            }))
        } else {
            this.setState({
                questionMode: newMode,
                editedQuestionIndex: editedIndex,
                question: JSON.parse(JSON.stringify(this.emptyQuestion))
            })
        }
    }

    handleAddCategory = () => {
        const { newCategoryName } = this.state;
        this.closeCategoryModal();

        if (newCategoryName.trim().length === 0)
            return ;

        const body = {
            name: newCategoryName
        };

        api
            .post('/quizCategories', body)
            .then(() => this.closeCategoryModal())
            .then(() => this.setState({ newCategoryName: '' }))
            .then(() => this.fetchCategories());
    }

    showCategoryModal = () => {
        this.setState({
            categoryModalVisible: true
        })
    }
    closeCategoryModal = () => {
        this.setState({
            categoryModalVisible: false
        })
    }
    
    beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
            return false;
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
            return false;
        }
        this.uploadQuestionImage(file)
        return true;
    }

    uploadQuestionImage = (image) => {
        this.setState({ uploadingImage: true })
        
        var formData = new FormData();
        formData.append("questionImage", image);

        api
            .post('/upload', formData)
            .then(res => this.onQuestionChange('imgUrl', res.data))
            .finally(() => this.setState({ uploadingImage: false }));
    }

    emptyQuestion = {
        id: null,
        content: '',
        imgUrl: null,
        predefinedAnswers: true,
        answers: [
            {content: 'Odpowiedź 1', isCorrect: false},
            {content: 'Odpowiedź 2', isCorrect: false},
            {content: 'Odpowiedź 3', isCorrect: false},
            {content: 'Odpowiedź 4', isCorrect: true},
        ],
        timeForAnswer: 10,
        answerExplanation: '',
        points: 1
    };

    initialBasicInformation = {
        name: '',
        category: '',
        isPublic: false,
        isPublished: false
    };
    
    initialAdditionalMaterials = {
        basicHelp: '',
        files: []
    };
};