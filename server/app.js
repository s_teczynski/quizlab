const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const helmet = require("helmet");
const morgan = require('morgan');

const env = process.env.NODE_ENV || 'development';

require('dotenv').config({ path: `.env.${env}` });
require('dotenv').config({ path: '.env' });

app.use(helmet());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require("./routes")(app);

app.listen(process.env.APP_PORT, () => console.log(`Listening on port http://localhost:${process.env.APP_PORT}`))