const express = require('express');
const router = express.Router();
const { QuizResult, User } = require('../models');
const { loggedIn } = require("../helpers/auth.middleware");

router.get('/quiz/:quizId', loggedIn, async (req, res) => {
    const { quizId } = req.params;

    const results = await QuizResult.findAll({ 
        where: { quizId: quizId },
        include: [{ model: User, attributes: ['id', 'username', 'email']}],
        order: [
            ['points', 'DESC']
        ]
    });
    console.log(results[0].User);
    return res.status(200).send(results);
})

module.exports = router;