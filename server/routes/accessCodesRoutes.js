const express = require('express');
const router = express.Router();
const { v4: uuidv4 } = require('uuid');
const moment = require('moment');
const { Quiz, AccessCode } = require('../models');
const { loggedIn } = require("../helpers/auth.middleware");

router.post('/quiz/:id', loggedIn, async (req, res) => {
    const { id } = req.params; 
    const quiz = await Quiz.findByPk(id);

    if (!quiz)
        return res.status(400).send();
    
    if (quiz.authorId !== req.user.id)
        return res.status(400).send();

    const newAccessCode = await AccessCode.create({
        id: uuidv4(),
        quizId: id,
        accessCode: generateRandomAlphanumericalString(6),
        validUntil: moment().add(4, 'h')
    });

    return res.status(200).send(newAccessCode.accessCode)
})

const generateRandomAlphanumericalString = (length) => {
    const base = 'ABCDEFGHIJKLMNOUPRSTUWXZ1234567890';
    let alphaString = '';
    for (let i=0; i<length; ++i) {
        const randomInt = Math.floor(Math.random() * base.length);
        alphaString += base[randomInt];
    }
    return alphaString;
}

module.exports = router;