const express = require('express');
const router = express.Router();
const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const emailValidator = require("email-validator");
const { User } = require('../models');

router.post('/register', async (req, res, next) => {
    const { email, password, username } = req.body;

    if (!emailValidator.validate(email) || !password || !username)
        return res.status(400).json({ message: 'Form has not been correctly filled.'})

    if (await doesEmailExist(email))
        return res.status(400).json({ message: 'Email already exists.' })


    if (await doesUsernameExist(username))
        return res.status(400).json({ message: 'Username already exists.' })

    const hashedPassword = await bcrypt.hash(password, 10);

    try {
        const user = await User.create({ 
            id: uuidv4(),
            email: email, 
            password: hashedPassword,
            username: username,
            avatar: null // dummy
        });
        res.status(201).send(user);
    } catch (err) {
        next(err);
    }
})

router.post('/login', async (req, res) => {
    const { email, password } = req.body;
    const user = await User.findOne({
        where: { email: email }
    });

    if (!user) 
        return res.status(400).json({ message: 'User with this email has not been found.' })

    const validPassword = await bcrypt.compare(password, user.password);

    if (!validPassword)
        return res.status(400).json({ message: 'Invalid password.' })

    const token = jwt.sign({ id: user.id, email: email }, process.env.JWT_SECRET, { expiresIn: '2h' });

    return res.header("auth-token", token).send(token);

})

const doesEmailExist = async (email) => {
    const user = await User.findOne({
        where: { email: email }
    });

    return user !== null;
}

const doesUsernameExist = async (username) => {
    const user = await User.findOne({
        where: { username: username }
    });

    return user !== null;
}

module.exports = router;