const express = require('express');
const router = express.Router();
const { Quiz, AccessCode, QuizParticipant } = require('../models');
const { loggedIn } = require("../helpers/auth.middleware");

router.post('/', loggedIn, async (req, res) => {
    const { id } = req.user;
    const { quizId, accessCode } = req.body;

    if (accessCode) {
        const accessedCodeInfo = await AccessCode.findOne({ where: { accessCode: accessCode }});
        if (!accessedCodeInfo)
            return res.status(400).send();
        
        const newParticipant = await QuizParticipant.create({
            userId: id,
            quizId: accessedCodeInfo.quizId,
            finished: false
        });

        return res.status(201).send(newParticipant);
    }

    const quiz = await Quiz.findByPk(quizId);

    if (!quiz || !quiz.isPublic)
        return res.status(400).send();

    const newParticipant = await QuizParticipant.create({
        userId: id,
        quizId: quizId,
        finished: false
    });

    return res.status(201).send(newParticipant);
})

module.exports = router;