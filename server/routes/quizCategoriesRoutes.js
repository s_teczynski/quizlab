const express = require('express');
const router = express.Router();
const { v4: uuidv4 } = require('uuid');
const { QuizCategory } = require('../models');

router.get('/', async (req, res) => {
    const categories = await QuizCategory.findAll({
        order: [
            ['quizes_amount', 'DESC']
        ]
    });
    return res.status(200).send(categories);
})

router.post('/', async (req, res) => {
    const { name } = req.body;
            
    if (!name) 
        return res.status(400).send();
        
    if (await doesCategoryExist(name))
        return res.status(400).send();

    const category = await QuizCategory.create({
        id: uuidv4(),
        name: name,
        quizesAmount: 0
    })

    return res.status(201).send(category);
})

const doesCategoryExist = async (categoryName) => {
    const category = await QuizCategory.findOne({ where: { name: categoryName }});
    return category !== null;
}

module.exports = router;