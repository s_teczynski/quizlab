const express = require('express');
const router = express.Router();
const { Question, Answer } = require('../models');
const { loggedIn } = require("../helpers/auth.middleware");

router.get('/:quizId/random', loggedIn, async (req, res) => {
    const { id } = req.user;
    const { quizId } = req.params;

    const answeredQuestionsDb = await Answer.findAll({ attributes: ['question_id'], where: { quizId: quizId, userId: id}, raw: true });
    const allQuestionsDb = await Question.findAll({ attributes: ['id'], where: { quizId: quizId }, raw: true });
    const answeredQuestions = Array.from(answeredQuestionsDb).map(el => el.question_id);
    const allQuestions = Array.from(allQuestionsDb).map(el => el.id);
    const notAnsweredQuestions = allQuestions.filter(el => answeredQuestions.indexOf(el) < 0);
    
    if (notAnsweredQuestions.length === 0)
        return res.status(404).send('All questions in this quiz has been answered');
    
    const question = await Question.findByPk(notAnsweredQuestions[0]);
    const stats = {
        answeredQuestions: answeredQuestions.length,
        allQuestions: allQuestions.length
    }

    return res.status(200).send({ question, stats });
})

module.exports = router;