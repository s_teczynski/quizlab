const authRoutes = require('./authRoutes');
const quizesRoutes = require('./quizesRoutes');
const questionsRoutes = require('./questionsRoutes');
const accessCodesRoutes = require('./accessCodesRoutes');
const answersRoutes = require('./answersRoutes');
const quizParticipantsRoutes = require('./quizParticipantsRoutes');
const quizResultsRoutes = require('./quizResultsRoutes');
const quizCategoriesRoutes = require('./quizCategoriesRoutes');
const uploadRoutes = require('./uploadRoutes');

module.exports = app => {
    app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });
    
    app
        .use('/', authRoutes)
        .use('/quizes', quizesRoutes)
        .use('/questions', questionsRoutes)
        .use('/accessCode', accessCodesRoutes)
        .use('/answers', answersRoutes)
        .use('/quizParticipants', quizParticipantsRoutes)
        .use('/quizResults', quizResultsRoutes)
        .use('/quizCategories', quizCategoriesRoutes)
        .use('/upload', uploadRoutes);
}