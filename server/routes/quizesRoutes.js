const express = require('express');
const router = express.Router();
const { v4: uuidv4 } = require('uuid');
const { Quiz, Question, QuizParticipant, QuizCategory } = require('../models');
const { addUser, loggedIn } = require("../helpers/auth.middleware");

router.get('/', addUser, async (req, res) => {        
    const userId = req.user && req.user.id;
    
    if (!userId) {
        const quizes = await Quiz.findAll();
        return res.status(200).send(quizes);
    }
    
    const quizes = await Quiz.findAll({
        include: [
            {
                model: QuizParticipant,
                attributes: ['finished'],
                where: { userId: userId },
                required: false,
            },
        ],
        raw: true
    })

    const quizesResponse = quizes.filter(quiz => quiz.isPublished && (quiz.isPublic || quiz['QuizParticipants.finished'] !== null));

    return res.status(200).send(quizesResponse);
})

router.post('/', loggedIn, async (req, res) => {
    const { basicInformation, additionalMaterials, questions } = req.body;
    const { id } = req.user;

    const quizId = uuidv4();
    const quiz = await Quiz.create({
        id: quizId,
        authorId: id,
        categoryId: basicInformation.category,
        name: basicInformation.name,
        additionalHelp: additionalMaterials.basicHelp,
        additionalFiles: null, // dummy
        isPublic: basicInformation.isPublic,
        isPublished: basicInformation.isPublished
    });

    questions.forEach(async question => {
        
        await Question.create({
            id: uuidv4(),
            quizId: quizId,
            questionText: question.content,
            questionPhoto: question.imgUrl,
            answers: question.answers,
            plainInput: !question.predefinedAnswers,
            timeForAnswer: question.timeForAnswer,
            answerExplanation: question.answerExplanation,
            points: question.points,
            order: 1 // dummy
        });
    })

    updateCategoryQuizesAmount(quiz.categoryId);
    return res.status(201).send(quiz);
})

router.patch('/:id', async (req, res) => {
    const { id } = req.params;
    const { basicInformation, additionalMaterials, questions } = req.body;
    
    const quiz = await Quiz.findByPk(id);
    if (!quiz) {
        return res.status(404).send();
    }

    await quiz.update({
        name: basicInformation.name,
        additionalHelp: additionalMaterials.basicHelp,
        additionalFiles: null, // dummy
        isPublic: basicInformation.isPublic,
        isPublished: basicInformation.isPublished
    });

    questions.forEach(async question => {
        if (question.id) {
            const questionDb = await Question.findByPk(question.id);
            await questionDb.update({
                questionText: question.content,
                questionPhoto: question.imgUrl,
                answers: question.answers,
                plainInput: !question.predefinedAnswers,
                timeForAnswer: question.timeForAnswer,
                answerExplanation: question.answerExplanation,
                points: question.points,
                order: 1 // dummy
            });
        } else {
            await Question.create({
                id: uuidv4(),
                quizId: id,
                questionText: question.content,
                questionPhoto: question.imgUrl,
                answers: question.answers,
                plainInput: !question.predefinedAnswers,
                timeForAnswer: question.timeForAnswer,
                answerExplanation: question.answerExplanation,
                points: question.points,
                order: 1 // dummy
            });
        }
    });

    return res.send();
})

router.get('/mine', loggedIn, async (req, res) => {        
    const { id } = req.user;

    const quizes = await Quiz.findAll({where: { authorId: id }});
    return res.status(200).send(quizes);
})

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    
    const quiz = await Quiz.findByPk(id);
    const questions = await Question.findAll({ where: { quizId: id } });

    return res.status(200).send({ quiz, questions })
})

const updateCategoryQuizesAmount = async (categoryId) => {
    const category = await QuizCategory.findByPk(categoryId);

    if (!category)
        return ;

    const categoryQuizes = await Quiz.findAll({ where: { categoryId: categoryId }});

    await category.update({
        quizesAmount: categoryQuizes.length
    });
}

module.exports = router;