const express = require('express');
const router = express.Router();
const { v4: uuidv4 } = require('uuid');
const { Quiz, Question, Answer, QuizParticipant, QuizResult } = require('../models');
const { addUser, loggedIn } = require("../helpers/auth.middleware");

router.get('/', loggedIn, async (req, res) => {
    const { id } = req.user;
    const answers = await Answer.findAll({
        where: {userId: id},
        include: [Question, Quiz],
    });
    return res.send(answers);
})

router.post('/', addUser, async (req, res) => {
    const userId = req.user && req.user.id;
    const { questionId, quizId, answer, isCorrect } = req.body;

    const newAnswer = await Answer.create({
        id: uuidv4(),
        quizId: quizId,
        questionId: questionId,
        userId: userId,
        answer: answer,
        isCorrect: isCorrect
    });

    if (await isQuizFinished(quizId, userId)) {
        await updateDataAfterFinishingQuiz(quizId, userId);
    }

    return res.status(201).send(newAnswer);
})

const isQuizFinished = async (quizId, userId) => {
    if (!userId)
        return false;
    
    const questionsAnswered = await Answer.findAll({ where: { userId: userId, quizId: quizId } });
    const allQuestions = await Question.findAll({ where: { quizId: quizId }});

    return questionsAnswered.length === allQuestions.length;
}

const updateDataAfterFinishingQuiz = async (quizId, userId) => {
    await updateParticipant(quizId, userId);
    await createTestResult(quizId, userId);
}

const updateParticipant = async (quizId, userId) => {
    const participant = await QuizParticipant.findOne({ where: { userId: userId, quizId: quizId } });
    if (!participant) 
        return ;

    await participant.update({
        finished: true
    });
}

const createTestResult = async (quizId, userId) => {
    let allPoints = 0;
    let userPoints = 0;
    
    const answers = await Answer.findAll({ where: { userId: userId, quizId: quizId }, raw: true });
    const questions = await Question.findAll({ where: { quizId: quizId }, raw: true});

    questions.forEach(question => {
        const answer = answers.find(a => a.questionId === question.id);
        if (!answer) {
            return ;
        }

        allPoints += question.points;
        userPoints += answer.isCorrect === "true" ? question.points : 0;
    })

    await QuizResult.create({
        id: uuidv4(),
        quizId: quizId,
        userId: userId,
        points: userPoints,
        maxPoints: allPoints
    });
}

module.exports = router;