'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'answers',
      'user_id', {
        type: Sequelize.UUID,
        field: "user_id" 
      }, 
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('answers', 'user_id');
  }
};
