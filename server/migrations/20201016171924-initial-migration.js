'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("access_codes", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
      },
      quizId: {
        type: Sequelize.UUID,
        field: 'quiz_id'
      },
      accessCode: {
        type: Sequelize.STRING,
        field: "access_code"
      },
      validUntil: {
        type: Sequelize.DATE,
        field: "valid_until"
      },
    });
    await queryInterface.createTable("answers", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
      },
      quizId: {
        type: Sequelize.UUID,
        field: "quiz_id"
      },
      questionId: {
        type: Sequelize.UUID,
        field: "question_id"
      },
      answer: Sequelize.STRING,
      isCorrect: {
        type: Sequelize.STRING,
        field: "is_correct"
      }
    });
    await queryInterface.createTable("likes", {
      userId: {
        type: Sequelize.UUID,
        field: 'user_id'
      },
      authorId: {
        type: Sequelize.UUID,
        field: 'author_id'
      },
    });
    await queryInterface.createTable("posts", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
      },
      quizId: {
        type: Sequelize.UUID,
        field: 'quiz_id'
      },
      authorId: {
        type: Sequelize.UUID,
        field: 'author_id'
      },
      content: Sequelize.STRING,
      respondedPostId: {
        type: Sequelize.UUID,
        field: 'responded_post_id'
      },
      createdAt: {
        type: Sequelize.DATE,
        field: 'created_at'
      }
    });
    await queryInterface.createTable("questions", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
      },
      quizId: {
        type: Sequelize.UUID,
        field: "quiz_id"
      },
      questionText: {
        type: Sequelize.STRING,
        field: "question_text"
      },
      questionPhoto: {
        type: Sequelize.STRING,
        field: "question_photo"
      },
      answers: Sequelize.JSON,
      plainInput: {
        type: Sequelize.BOOLEAN,
        field: "plain_input"
      },
      timeForAnswer: {
        type: Sequelize.INTEGER,
        field: "time_for_answer"
      },
      answerExplanation: {
        type: Sequelize.STRING,
        field: "answer_explanation"
      },
      points: Sequelize.INTEGER,
      order: Sequelize.INTEGER
    });
    await queryInterface.createTable("quizes", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
      },
      authorId: {
        type: Sequelize.UUID,
        field: "author_id"
      },
      categoryId: {
        type: Sequelize.UUID,
        field: "category_id"
      },
      name: Sequelize.STRING,
      additionalHelp: {
        type: Sequelize.STRING,
        field: "additional_help"
      },
      additionalFiles: {
        type: Sequelize.JSON,
        field: "additional_files"
      },
      isPublic: {
        type: Sequelize.BOOLEAN,
        field: "is_public"
      },
      isPublished: {
        type: Sequelize.BOOLEAN,
        field: "is_published"
      }
    });
    await queryInterface.createTable("quiz_categories", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
      },
      name: Sequelize.STRING,
      quizesAmount: {
        type: Sequelize.INTEGER,
        field: "quizes_amount"
      }
    });
    await queryInterface.createTable("quiz_participants", {
      userId: {
        type: Sequelize.UUID,
        field: 'user_id'
      },
      quizId: {
        type: Sequelize.UUID,
        field: 'quiz_id'
      },
    });
    await queryInterface.createTable("quiz_results", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
      },
      quizId: {
        type: Sequelize.UUID,
        field: 'quiz_id'
      },
      userId: {
        type: Sequelize.UUID,
        field: "user_id"
      },
      points: Sequelize.INTEGER,
      maxPoints: {
        type: Sequelize.INTEGER,
        field: 'max_points'
      }
    });
    await queryInterface.createTable("users", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
      },
      username: Sequelize.STRING,
      password: Sequelize.STRING,
      email: Sequelize.STRING,
      avatar: Sequelize.STRING,
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("access_codes");
    await queryInterface.dropTable("answers");
    await queryInterface.dropTable("likes");
    await queryInterface.dropTable("posts");
    await queryInterface.dropTable("questions");
    await queryInterface.dropTable("quizes");
    await queryInterface.dropTable("quiz_categories");
    await queryInterface.dropTable("quiz_participants");
    await queryInterface.dropTable("quiz_results");
    await queryInterface.dropTable("users");
  }
};
