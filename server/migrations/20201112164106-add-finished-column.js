'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'quiz_participants',
      'finished', {
        type: Sequelize.BOOLEAN,
      }, 
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('quiz_participants', 'finished');
  }
};
