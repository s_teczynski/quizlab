"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class QuizCategory extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    QuizCategory.init(
        {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
            },
            name: DataTypes.STRING,
            quizesAmount: {
                type: DataTypes.INTEGER,
                field: "quizes_amount" 
            } 
        },
        {
            sequelize,
            timestamps: false,
            modelName: "QuizCategory",
            tableName: "quiz_categories",
        }
    );
    return QuizCategory;
};

