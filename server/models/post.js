"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Post extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Post.init(
        {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
            },
            quizId: {
                type: DataTypes.UUID,
                field: 'quiz_id'
            },
            authorId: {
                type: DataTypes.UUID,
                field: 'author_id'
            },
            content: DataTypes.STRING,
            respondedPostId: {
                type: DataTypes.UUID,
                field: 'responded_post_id'
            },
            createdAt: {
                type: DataTypes.DATE,
                field: 'created_at'
            }
        },
        {
            sequelize,
            timestamps: false,
            modelName: "Post",
            tableName: "posts",
        }
    );
    return Post;
};

