"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        static associate(models) {
            models.User.hasMany(models.QuizParticipant, {foreignKey: 'user_id'});
            models.User.hasMany(models.QuizResult, {foreignKey: 'user_id'});
        }
    }
    User.init(
        {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
            },
            username: DataTypes.STRING,
            password: DataTypes.STRING,
            email: DataTypes.STRING,
            avatar: DataTypes.STRING,
        },
        {
            sequelize,
            timestamps: false,
            modelName: "User",
            tableName: "users",
        }
    );
    return User;
};

