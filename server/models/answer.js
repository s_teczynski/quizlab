"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Answer extends Model {
        static associate(models) {
            models.Answer.belongsTo(models.Quiz, {foreignKey: 'quiz_id'});
            models.Answer.belongsTo(models.Question, {foreignKey: 'question_id'});
        }
    }
    Answer.init(
        {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
            },
            quizId: {
                type: DataTypes.UUID,
                field: "quiz_id" 
            }, 
            userId: {
                type: DataTypes.UUID,
                field: "user_id" 
            }, 
            questionId: {
                type: DataTypes.UUID,
                field: "question_id"
            }, 
            answer: DataTypes.STRING,
            isCorrect: {
                type: DataTypes.STRING,
                field: "is_correct" 
            }
        },
        {
            sequelize,
            timestamps: false,
            modelName: "Answer",
            tableName: "answers",
        }
    );
    return Answer;
};

