"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Question extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            models.Question.hasMany(models.Answer, {foreignKey: 'question_id'});
        }
    }
    Question.init(
        {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
            },
            quizId: {
                type: DataTypes.UUID,
                field: "quiz_id" 
            },
            questionText: {
                type: DataTypes.STRING,
                field: "question_text" 
            }, 
            questionPhoto: {
                type: DataTypes.STRING,
                field: "question_photo" 
            }, 
            answers: DataTypes.JSON,
            plainInput: {
                type: DataTypes.BOOLEAN,
                field: "plain_input" 
            },
            timeForAnswer: {
                type: DataTypes.INTEGER,
                field: "time_for_answer" 
            }, 
            answerExplanation: {
                type: DataTypes.STRING,
                field: "answer_explanation" 
            }, 
            points: DataTypes.INTEGER,
            order: DataTypes.INTEGER
        },
        {
            sequelize,
            timestamps: false,
            modelName: "Question",
            tableName: "questions",
        }
    );
    return Question;
};

