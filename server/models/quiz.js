"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Quiz extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            models.Quiz.hasMany(models.Answer, {foreignKey: 'quiz_id'});
            models.Quiz.hasMany(models.QuizParticipant, {foreignKey: 'quiz_id'});
        }
    }
    Quiz.init(
        {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
            },
            authorId: {
                type: DataTypes.UUID,
                field: "author_id" 
            },
            categoryId: {
                type: DataTypes.UUID,
                field: "category_id" 
            },
            name: DataTypes.STRING,
            additionalHelp: {
                type: DataTypes.STRING,
                field: "additional_help" 
            }, 
            additionalFiles: {
                type: DataTypes.JSON,
                field: "additional_files" 
            }, 
            isPublic: {
                type: DataTypes.BOOLEAN,
                field: "is_public" 
            }, 
            isPublished: {
                type: DataTypes.BOOLEAN,
                field: "is_published" 
            }
        },
        {
            sequelize,
            timestamps: false,
            modelName: "Quiz",
            tableName: "quizes",
        }
    );
    return Quiz;
};

