"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class QuizResult extends Model {
        static associate(models) {
            models.QuizResult.belongsTo(models.User, {foreignKey: 'user_id'});
        }
    }
    QuizResult.init(
        {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
            },
            quizId: {
                type: DataTypes.UUID,
                field: 'quiz_id'
            },
            userId: {
                type: DataTypes.UUID,
                field: "user_id" 
            },
            points: DataTypes.INTEGER,
            maxPoints: {
                type: DataTypes.INTEGER,
                field: 'max_points'
            }
        },
        {
            sequelize,
            timestamps: false,
            modelName: "QuizResult",
            tableName: "quiz_results",
        }
    );
    return QuizResult;
};

