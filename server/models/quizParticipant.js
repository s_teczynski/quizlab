"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class QuizParticipant extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            models.QuizParticipant.belongsTo(models.Quiz, {foreignKey: 'quiz_id'});
            models.QuizParticipant.belongsTo(models.User, {foreignKey: 'user_id'});
        }
    }
    QuizParticipant.init(
        {
            userId: {
                type: DataTypes.UUID,
                field: 'user_id',
                primaryKey: true
            },
            quizId: {
                type: DataTypes.UUID,
                field: 'quiz_id',
                primaryKey: true
            },
            finished: DataTypes.BOOLEAN
        },
        {
            sequelize,
            timestamps: false,
            modelName: "QuizParticipant",
            tableName: "quiz_participants",
        }
    );
    return QuizParticipant;
};

