"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class AccessCode extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    AccessCode.init(
        {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
            },
            quizId: {
                type: DataTypes.UUID,
                field: 'quiz_id'
            },
            accessCode: {
                type: DataTypes.STRING,
                field: "access_code" 
            },
            validUntil: {
                type: DataTypes.DATE,
                field: "valid_until" 
            },
        },
        {
            sequelize,
            timestamps: false,
            modelName: "AccessCode",
            tableName: "access_codes",
        }
    );
    return AccessCode;
};

