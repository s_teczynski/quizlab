CREATE TABLE public.access_codes
(
    id uuid NOT NULL,
    quiz_id uuid,
    access_code character varying(255) COLLATE pg_catalog."default",
    valid_until timestamp with time zone,
    CONSTRAINT access_codes_pkey PRIMARY KEY (id)
);

CREATE TABLE public.answers
(
    id uuid NOT NULL,
    quiz_id uuid,
    question_id uuid,
    answer character varying(255) COLLATE pg_catalog."default",
    is_correct character varying(255) COLLATE pg_catalog."default",
    user_id uuid,
    CONSTRAINT answers_pkey PRIMARY KEY (id)
);

CREATE TABLE public.likes
(
    user_id uuid,
    author_id uuid
);

CREATE TABLE public.posts
(
    id uuid NOT NULL,
    quiz_id uuid,
    author_id uuid,
    content character varying(255) COLLATE pg_catalog."default",
    responded_post_id uuid,
    created_at timestamp with time zone,
    CONSTRAINT posts_pkey PRIMARY KEY (id)
);

CREATE TABLE public.questions
(
    id uuid NOT NULL,
    quiz_id uuid,
    question_text character varying(255) COLLATE pg_catalog."default",
    question_photo character varying(255) COLLATE pg_catalog."default",
    answers json,
    plain_input boolean,
    time_for_answer integer,
    answer_explanation character varying(255) COLLATE pg_catalog."default",
    points integer,
    "order" integer,
    CONSTRAINT questions_pkey PRIMARY KEY (id)
);

CREATE TABLE public.quiz_categories
(
    id uuid NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    quizes_amount integer,
    CONSTRAINT quiz_categories_pkey PRIMARY KEY (id)
);

CREATE TABLE public.quiz_participants
(
    user_id uuid,
    quiz_id uuid,
    finished boolean
);

CREATE TABLE public.quiz_results
(
    id uuid NOT NULL,
    quiz_id uuid,
    user_id uuid,
    points integer,
    max_points integer,
    CONSTRAINT quiz_results_pkey PRIMARY KEY (id)
);

CREATE TABLE public.quizes
(
    id uuid NOT NULL,
    author_id uuid,
    category_id uuid,
    name character varying(255) COLLATE pg_catalog."default",
    additional_help character varying(255) COLLATE pg_catalog."default",
    additional_files json,
    is_public boolean,
    is_published boolean,
    CONSTRAINT quizes_pkey PRIMARY KEY (id)
);

CREATE TABLE public.users
(
    id uuid NOT NULL,
    username character varying(255) COLLATE pg_catalog."default",
    password character varying(255) COLLATE pg_catalog."default",
    email character varying(255) COLLATE pg_catalog."default",
    avatar character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT users_pkey PRIMARY KEY (id)
);