class Quiz {
  String id;
  String authorId;
  String categoryId;
  String name;
  String additionalHelp;
  List<String> additionalFiles;
  bool isPublic;
  bool isPublished;
  bool finished;

  Quiz({this.id, this.authorId, this.categoryId, this.name, this.additionalHelp, this.additionalFiles, this.isPublic, this.isPublished, this.finished});

  factory Quiz.fromJson(Map<String, dynamic> quizJson) => Quiz(
    id: quizJson["id"],
    authorId: quizJson["authorId"],
    categoryId: quizJson["categoryId"],
    name: quizJson["name"],
    additionalHelp: quizJson["additionalHelp"],
    additionalFiles: quizJson["additionalFiles"],
    isPublic: quizJson["isPublic"],
    isPublished: quizJson["isPublished"],
    finished: quizJson["QuizParticipants.finished"],
  );

}

class Answer {
  String content;
  bool isCorrect;

  Answer({this.content, this.isCorrect});

  factory Answer.fromJson(Map<String, dynamic> answerJson) => Answer(
    content: answerJson["content"],
    isCorrect: answerJson["isCorrect"],
  );
}

class Answers {
  String answers;
  Answers({this.answers});

  factory Answers.fromJson(Map<String, dynamic> answerJson) => Answers(
    answers: answerJson["answers"],
  );
}

class Question {
  String id;
  String quizId;
  String questionText;
  String questionPhoto;
  List<Answer> answers;
  bool plainInput;
  int timeForAnswer;
  String answerExplanation;
  int points;
  int order;

  Question({this.id, this.quizId, this.questionText, this.questionPhoto, this.answers, this.plainInput, this.timeForAnswer, this.answerExplanation, this.points, this.order });

  factory Question.fromJson(Map<String, dynamic> questionJson) => Question(
    id: questionJson["id"],
    quizId: questionJson["quizId"],
    questionText: questionJson["questionText"],
    questionPhoto: questionJson["questionPhoto"],
    answers: questionJson["answers"] == null ? null : List<Answer>.from(questionJson["answers"].map((x) => Answer.fromJson(x))),
    plainInput: questionJson["plainInput"],
    timeForAnswer: questionJson["timeForAnswer"],
    answerExplanation: questionJson["answerExplanation"],
    points: questionJson["points"],
    order: questionJson["order"],
  );
}

class DbAnswer {
  String id;
  String answer;
  bool isCorrect;
  Question question;
  Quiz quiz;

  DbAnswer({this.id, this.answer, this.isCorrect, this.question, this.quiz });

  factory DbAnswer.fromJson(Map<String, dynamic> answerJson) => DbAnswer(
    id: answerJson["id"],
    answer: answerJson["answer"],
    isCorrect: answerJson["isCorrect"] == 'true',
    question: answerJson["Question"] == null ? null : Question.fromJson(answerJson["Question"]),
    quiz: answerJson["Quiz"] == null ? null : Quiz.fromJson(answerJson["Quiz"]),
  );
}

class Stats {
  int answeredQuestions;
  int allQuestions;

  Stats({ this.answeredQuestions, this.allQuestions });

  factory Stats.fromJson(Map<String, dynamic> statsJson) => Stats(
    answeredQuestions: statsJson["answeredQuestions"],
    allQuestions: statsJson["allQuestions"]
  );
}

class User {
  String id;
  String username;
  String email;

  User({ this.id, this.username, this.email });

  factory User.fromJson(Map<String, dynamic> userJson) => User(
    id: userJson["id"],
    username: userJson["username"],
    email: userJson["email"]
  );
}

class QuizResult {
  String id;
  String quizId;
  String userId;
  int points;
  int maxPoints;
  User user;

  QuizResult({this.id, this.quizId, this.userId, this.points, this.maxPoints, this.user });

  factory QuizResult.fromJson(Map<String, dynamic> answerJson) => QuizResult(
    id: answerJson["id"],
    quizId: answerJson["quizId"],
    userId: answerJson["userId"],
    points: answerJson["points"],
    maxPoints: answerJson["maxPoints"],
    user: answerJson["User"] == null ? null : User.fromJson(answerJson["User"])
  );
}

class AnswersFilters {
  String quizName;

  AnswersFilters(this.quizName);
}