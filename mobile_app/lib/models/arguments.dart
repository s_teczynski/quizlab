import 'package:mobile_app/models/models.dart';

class QuizArguments {
  final Quiz quiz;

  QuizArguments(this.quiz);
}