import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:mobile_app/models/arguments.dart';
import 'package:mobile_app/models/models.dart' as models;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:io';
import 'package:mobile_app/helpers/helpers.dart';
import 'package:mobile_app/widgets/custom_appbar.dart';
import 'package:mobile_app/widgets/custom_drawer.dart';

class QuizesCard extends StatefulWidget {
  const QuizesCard(this.handleLogout) : super();

  final handleLogout;

  @override
  _QuizesCardState createState() => _QuizesCardState(this.handleLogout);
}

class _QuizesCardState extends State<QuizesCard> {
  _QuizesCardState(this.handleLogout);

  List<models.Quiz> _quizes = [];
  final storage = new FlutterSecureStorage();
  final _formKey = GlobalKey<FormState>();
  final key = new GlobalKey<ScaffoldState>();
  TextEditingController accessCodeController = TextEditingController();
  final handleLogout;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  void fetchData() async {
    String token = await storage.read(key: 'token');
    Response response = await get(
      '${getApiUrl()}/quizes',
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    final List data = jsonDecode(response.body);
    List<models.Quiz> list = data.map((val) => models.Quiz.fromJson(val)).toList();

    setState(() {
      _quizes = list;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(length: 3,
        child: Scaffold(
          key: key,
            appBar: CustomAppBar(
              bottom: TabBar(
                tabs: [new Text("Wszystkie"), new Text("W trakcie"), new Text("Ukończone")],
              ),
              actionIcon: IconButton(
                icon: Icon(Icons.filter_alt_outlined),
                onPressed: () { print('sada'); },
              )
            ),
          drawer: CustomDrawer(this.handleLogout),
          backgroundColor: Colors.grey[900],
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () { _showJoinDialog(); },
          ),
          body: TabBarView(
              children: [
                QuizesList(_quizes.where((q) => q.finished == null).toList(), false, fetchData),
                QuizesList(_quizes.where((q) => q.finished == false).toList(), false, fetchData),
                QuizesList(_quizes.where((q) => q.finished == true).toList(), true, fetchData)
              ]
          )
      )
    );
  }

  void _showJoinDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Dołącz do prywatnego quizu"),
          actions: [
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  validateAccessCode(accessCodeController.text);
                }
              },
              child: Text('Dołącz'),
            ),
          ],
          content: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: accessCodeController,
                  decoration: const InputDecoration(
                    hintText: 'Kod dostępu',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Podaj kod dostępu';
                    }
                    return null;
                  },
                ),
              ],
            ),
          )
        );
      },
    );
  }

  void validateAccessCode(String accessCode) async {
    Map data = {
      'accessCode': accessCode,
    };

    String body = jsonEncode(data);
    String token = await storage.read(key: 'token');

    Response response = await post(
        '${getApiUrl()}/quizParticipants',
        headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':  'Bearer $token'
        },
        body: body,
    );

    if (response.statusCode == 201) {
      Navigator.pop(context);
      key.currentState.showSnackBar(new SnackBar(
        content: new Text("Dołączono do prywatnego quizu."),
      ));
    }
  }
}

class QuizesList extends StatelessWidget {
  const QuizesList(this._quizes, this.isFinished, this.fetchData, {
    Key key,
  }) : super(key: key);

  final List<models.Quiz> _quizes;
  final bool isFinished;
  final void Function() fetchData;

  redirectToProperTab(context, quiz) {
    Navigator.pushNamed(
      context,
      isFinished ? '/finished_quiz' : '/quiz',
      arguments: QuizArguments(
        quiz,
      ),
    ).then((val) { this.fetchData(); });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
              children: _quizes.map((quiz) => Card(
                  child: Column(
                      children: <Widget> [
                        FlatButton(
                            onPressed: () { redirectToProperTab(context, quiz); },
                            child: Text(
                                quiz.name
                            )
                        )
                      ]
                  )
              )).toList()
          )
        ],
      ),
    );
  }
}

