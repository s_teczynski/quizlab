import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mobile_app/widgets/custom_appbar.dart';
import 'package:mobile_app/widgets/custom_drawer.dart';

class HomeView extends StatefulWidget {
  const HomeView(this.handleLogout) : super();

  final handleLogout;

  @override
  _HomeViewState createState() {
    return new _HomeViewState(this.handleLogout);
  }
}

class _HomeViewState extends State<HomeView> {
  _HomeViewState(this.handleLogout);

  final storage = new FlutterSecureStorage();
  final handleLogout;
  dynamic _user;

  @override
  void initState() {
    super.initState();
    getUserFromToken();
  }

  getUserFromToken() async {
    String token = await storage.read(key: 'token');
    setState(() {
      _user = JwtDecoder.decode(token);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      drawer: CustomDrawer(this.handleLogout),
      backgroundColor: Colors.grey[900],
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Strona domowa', style: TextStyle(fontSize: 20, color: Colors.white)),
              SizedBox(height: 20),
              Text(_user != null ? ('Witaj ' + _user['email']) : '', style: TextStyle(fontSize: 15, color: Colors.white))
            ],
          ),
        )
      ),
    );
  }
}