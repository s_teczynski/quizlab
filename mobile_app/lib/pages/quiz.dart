import 'package:flutter/material.dart';
import 'package:mobile_app/models/arguments.dart';
import 'package:mobile_app/models/models.dart' as models;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:mobile_app/helpers/helpers.dart';

class QuizView extends StatelessWidget {
  final QuizArguments quizArguments;
  final storage = new FlutterSecureStorage();
  QuizView(this.quizArguments);

  playQuiz(context) async {
    print(quizArguments.quiz.finished );
    if (quizArguments.quiz.finished == null) {
      await saveParticipant();
    }
    redirectToQuestion(context);
  }

  saveParticipant() async {
    Map data = {
      'quizId': quizArguments.quiz.id,
    };

    String body = jsonEncode(data);
    String token = await storage.read(key: 'token');

    await post(
        '${getApiUrl()}/quizParticipants',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization':  'Bearer $token'
        },
        body: body,
    );
  }

  redirectToQuestion(context) {
    Navigator.pushReplacementNamed(
      context,
      '/question',
      arguments: QuizArguments(
        quizArguments.quiz,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Quizlab'),
          centerTitle: true,
          backgroundColor: Colors.grey[850],
          elevation: 0.0,
        ),
        body: Padding(
            padding: EdgeInsets.all(20.0),
            child: SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Quiz id ' + quizArguments.quiz.id),
                    Text('Nazwa ' + quizArguments.quiz.name),
                    Text('Pomoc ' + quizArguments.quiz.additionalHelp),
                    Text('Publiczny? ' + quizArguments.quiz.isPublic.toString()),
                    Text('Opublikowany? ' + quizArguments.quiz.isPublished.toString()),
                    FlatButton(
                        onPressed: () { playQuiz(context); },
                        child: Text(quizArguments.quiz.finished == null ? 'Dołącz' : 'Kontynuuj'),
                        color: Colors.blueAccent
                    )
                  ],
                )
            )
        )
    );
  }
}
