import 'package:flutter/material.dart';
import 'package:mobile_app/models/arguments.dart';
import 'package:mobile_app/models/models.dart' as models;
import 'package:http/http.dart';
import 'dart:convert';
import 'dart:async';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mobile_app/helpers/helpers.dart';

class QuestionView extends StatefulWidget {
  final QuizArguments quizArguments;
  QuestionView(this.quizArguments);

  @override
  _QuestionViewState createState() => _QuestionViewState(quizArguments);
}

class _QuestionViewState extends State<QuestionView> {
  final storage = new FlutterSecureStorage();
  models.Question _question;
  models.Stats _stats;
  Timer _timer;
  int _timeLeft;

  final QuizArguments quizArguments;
  _QuestionViewState(this.quizArguments);

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  void fetchData() async {
    String token = await storage.read(key: 'token');
    Response response = await get(
        '${getApiUrl()}/questions/${quizArguments.quiz.id}/random',
        headers: <String, String>{
          'Authorization':  'Bearer $token'
        },
    );
    final data = jsonDecode(response.body);
    setState(() {
      _question = models.Question.fromJson(data['question']);
      _stats = models.Stats.fromJson(data['stats']);
      _timeLeft = _question.timeForAnswer * 100;
      startTimer();
    });
  }

  void startTimer() {
    const onePeriod = const Duration(milliseconds: 10);
    _timer = new Timer.periodic(
      onePeriod,
          (Timer timer) => setState(() {
          if (_timeLeft < 1) {
            timer.cancel();
            var answer = new models.Answer(
              content: '',
              isCorrect: false
            );
            _validateAnswer(answer);
          } else {
            _timeLeft = _timeLeft - 1;
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void _validateAnswer(answer) async {
    Map data = {
      'questionId': _question.id,
      'quizId': _question.quizId,
      'answer': answer.content,
      'isCorrect': answer.isCorrect
    };

    String body = jsonEncode(data);
    String token = await storage.read(key: 'token');

    await post(
      '${getApiUrl()}/answers',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':  'Bearer $token'
      },
      body: body,
    );

    _showDialog(answer.isCorrect);
  }

  void _showDialog(isCorrect) {
    _timer.cancel();
    if (_stats.answeredQuestions + 1 == _stats.allQuestions) {
      return _showFinalDialog(isCorrect);
    }

    showDialog(
      context: context,
      builder: (BuildContext context) {

        return AlertDialog(
          backgroundColor: isCorrect ? Colors.green[200] : Colors.red[200],
          title: new Text("Poprawność"),
          content: Column(
            children: [
              Text(isCorrect ? 'Poprawna odpowiedź' : 'Niepoprawna odpowiedź'),
              isCorrect
                  ? Text('')
                  : Text('Poprawna odpowiedź to:  ${_question.answers.firstWhere((element) => element.isCorrect).content}'),
              Text(_question.answerExplanation.length > 0
                  ? ('Wyjaśnienie: ' + _question.answerExplanation) : ''),
            ],
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Idź do kolejnego pytania"),
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushReplacementNamed(
                  context,
                  '/question',
                  arguments: QuizArguments(
                    quizArguments.quiz,
                  ),
                );
              },
            ),
          ],
        );
      },
    );
  }
  void _showFinalDialog(isCorrect) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Koniec"),
          content: Column(
            children: [
              Text('Koniec quizu'),
            ],
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Zobacz wynik"),
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushReplacementNamed(
                  context,
                  '/finished_quiz',
                  arguments: QuizArguments(
                    quizArguments.quiz,
                  ),
                );
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Quizlab'),
          centerTitle: true,
          backgroundColor: Colors.grey[850],
          elevation: 0.0,
        ),

        body: Padding(
            padding: EdgeInsets.all(20.0),
            child: SafeArea(
                  child: ListView(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                    children: <Widget>[
                      Text(_stats != null ? 'Pytanie ${_stats.answeredQuestions + 1} / ${_stats.allQuestions}' : ''),
                      Text(_question != null ?_question.questionText : ''),
                      _question != null && _question.questionPhoto != null ? Image.network(_question.questionPhoto) : SizedBox.shrink(),
                      GridView.count(
                          crossAxisCount: 2,
                          childAspectRatio: 3.0,
                          physics: NeverScrollableScrollPhysics(), // to disable GridView's scrolling
                          shrinkWrap: true,
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          mainAxisSpacing: 8.0,
                          crossAxisSpacing: 8.0,
                          children: _question != null ? _question.answers.map((answer) => FlatButton(
                              onPressed: () { _validateAnswer(answer); },
                              child: Text(answer.content),
                              color: Colors.amberAccent
                          )).toList() : []
                      ),
                      LinearProgressIndicator(
                          value: _timeLeft != null ? (_timeLeft / 100).toDouble() / _question.timeForAnswer : 0
                      ),
                      _timeLeft != null ? Text('Pozostało czasu: ${(_timeLeft / 100).ceil().toString()}s') : SizedBox.shrink()
                    ],
                  )
                )
        )
    );
  }
}
