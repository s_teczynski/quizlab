import 'package:flutter/material.dart';
import 'package:mobile_app/helpers/constants.dart';

class RegisterView extends StatefulWidget {
  Function onRegister;
  RegisterView({this.onRegister});

  @override
  _RegisterViewState createState() {
    return new _RegisterViewState(onRegister);
  }
}

class _RegisterViewState extends State<RegisterView> {
  Function onRegister;
  _RegisterViewState(this.onRegister);
  final _formKey = GlobalKey<FormState>();

  TextEditingController usernameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image(image: AssetImage('assets/logo.png')),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.8,
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: emailController,
                  decoration: const InputDecoration(
                    hintText: 'Email',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Podaj swój email';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: usernameController,
                  decoration: const InputDecoration(
                    hintText: 'Nick',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Podaj swój nick';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: passwordController,
                  decoration: const InputDecoration(
                    hintText: 'Hasło',
                  ),
                  obscureText: true,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Podaj swoje hasło';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      onRegister(emailController.text, usernameController.text, passwordController.text);
                    }
                  },
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Constants.secondaryColor)),
                  child: Text('Zarejestruj'),
                ),
              ],
            ),
          )
        )
      ]
    );
  }


}