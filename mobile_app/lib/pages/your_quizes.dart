import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:mobile_app/models/models.dart' as models;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mobile_app/helpers/helpers.dart';
import 'package:mobile_app/widgets/custom_appbar.dart';
import 'package:mobile_app/widgets/custom_drawer.dart';

class YourQuizesView extends StatefulWidget {
  const YourQuizesView(this.handleLogout) : super();

  final handleLogout;

  @override
  _YourQuizesViewState createState() => _YourQuizesViewState(this.handleLogout);
}

class _YourQuizesViewState extends State<YourQuizesView> {
  _YourQuizesViewState(this.handleLogout);

  List<models.Quiz> _quizes = [];
  final storage = new FlutterSecureStorage();
  final handleLogout;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  void fetchData() async {
    String token = await storage.read(key: 'token');
    Response response = await get(
      '${getApiUrl()}/quizes/mine',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':  'Bearer $token'
      },
    );

    final List data = jsonDecode(response.body);
    List<models.Quiz> list = data.map((val) => models.Quiz.fromJson(val)).toList();

    setState(() {
      _quizes = list;
    });
  }

  void _showQuestionDialog(content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Kod dostępu"),
          content: Column(
              children: [
                Text(content)
              ]
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      drawer: CustomDrawer(this.handleLogout),
      backgroundColor: Colors.grey[900],
      body: Padding(
        padding: const EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 0),
        child: Column(
          children: <Widget>[
            Text('Twoje quizy', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20)),
            Expanded(
                child: ListView(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    children: _quizes != null ? _quizes.map((quiz) => Card(
                            child: Padding(
                                padding: EdgeInsets.all(10),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget> [
                                      Text('Quiz: ${quiz.name}'),
                                      Text('Publiczny: ${quiz.isPublic}'),
                                      FlatButton(
                                          onPressed: () => generateAccessCode(quiz.id),
                                          color: Colors.greenAccent,
                                          child: Text('Wygeneruj kod dostępu')
                                      )
                                    ]
                                )
                            )
                        )
                    ).toList() : []
                )
            )
          ],
        ),
      ),
    );
  }
  
  generateAccessCode(quizId) async {
    String token = await storage.read(key: 'token');
    Response response = await post(
      '${getApiUrl()}/accessCode/quiz/$quizId',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':  'Bearer $token'
      },
    );
    if (response.statusCode == 200) {
      _showQuestionDialog(response.body);
    }
  }
}

