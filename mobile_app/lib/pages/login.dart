import 'package:flutter/material.dart';
import 'package:mobile_app/helpers/constants.dart';
import 'package:mobile_app/helpers/helpers.dart';

class LoginView extends StatefulWidget {
  Function onLogin;
  LoginView({this.onLogin});

  @override
  _LoginViewState createState() {
    return new _LoginViewState(onLogin);
  }
}

class _LoginViewState extends State<LoginView> {
  final void Function(String, String) onLogin;
  _LoginViewState(this.onLogin);
  final _formKey = GlobalKey<FormState>();

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image(image: AssetImage('assets/logo.png')),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.8,
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: emailController,
                  decoration: const InputDecoration(
                    hintText: 'Email',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Podaj swój email';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: passwordController,
                  decoration: const InputDecoration(
                    hintText: 'Hasło',
                  ),
                  obscureText: true,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Podaj swoje hasło';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      onLogin(emailController.text, passwordController.text);
                    }
                  },
                  // style: ButtonStyle(backgroundColor: Color.fromRGBO(23, 97, 160, 1)),
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Constants.secondaryColor)),
                  child: Text('Zaloguj'),
                ),

              ],
            ),
          )
        )
      ],
    );


  }


}