import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:mobile_app/models/models.dart' as models;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mobile_app/helpers/helpers.dart';
import 'package:mobile_app/widgets/custom_appbar.dart';
import 'package:mobile_app/widgets/custom_drawer.dart';
import 'package:mobile_app/widgets/custom_dialog.dart';

class AnswersView extends StatefulWidget {
  const AnswersView(this.handleLogout) : super();

  final handleLogout;

  @override
  _AnswersViewState createState() => _AnswersViewState(this.handleLogout);
}

class _AnswersViewState extends State<AnswersView> {
  _AnswersViewState(this.handleLogout);

  List<models.DbAnswer> _answers = [];
  List<String> _quizesNames = [];
  models.AnswersFilters filters = new models.AnswersFilters('');
  List<models.DbAnswer> _shownAnswers = [];

  final storage = new FlutterSecureStorage();
  final handleLogout;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  void fetchData() async {
    String token = await storage.read(key: 'token');
    Response response = await get(
      '${getApiUrl()}/answers',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':  'Bearer $token'
      },
    );

    final List data = jsonDecode(response.body);
    List<models.DbAnswer> list = data.map((val) => models.DbAnswer.fromJson(val)).toList();

    setState(() {
      _answers = list;
      _shownAnswers = list;
      _quizesNames = getQuizesNames(list);
    });
  }

  List<String> getQuizesNames(List<models.DbAnswer> answers) {
    List<String> quizesNames = answers.map((answer) => answer.quiz.name).toSet().toList();
    return quizesNames;
  }

  void _showFilterDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomDialog("Filtruj", _quizesNames, filters, onChangeFilters, onFilterClick);
      }
    );
  }

  void onChangeFilters(String quizName) {
    var newFilters = new models.AnswersFilters(quizName);

    setState(() {
      filters = newFilters;
      Navigator.pop(context);
      _showFilterDialog();
    });
  }

  void onFilterClick() {
    setState(() {
      _shownAnswers = _answers.where((answer) => answer.quiz.name == filters.quizName).toList();
    });
    Navigator.pop(context);
  }

  void _showQuestionDialog(content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Odpowiedź"),
          content: Column(
            children: [
              Text(content)
            ]
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: CustomAppBar(
          actionIcon: IconButton(
            icon: Icon(Icons.filter_alt_outlined),
            onPressed: _showFilterDialog,
          )
      ),
      drawer: CustomDrawer(this.handleLogout),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 0),
        child: Column(
          children: <Widget>[
            Text('Twoje odpowiedzi', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20)),
            Text(
                'Poprawne: ${_shownAnswers.where((a) => a.isCorrect).toList().length} / ${_shownAnswers.length}',
                style: TextStyle(color: Colors.white)
            ),
            Expanded(
              child: ListView(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  children: _shownAnswers != null ? _shownAnswers.map((answer) => GestureDetector(
                      onTap: () => _showQuestionDialog('${answer.question.questionText} ${answer.question.answerExplanation}'),
                      child: Card(
                        color: answer.isCorrect ? Colors.green : Colors.red,
                          child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget> [
                                    Text('Quiz: ${answer.quiz.name}'),
                                    Text('Pytanie: ${answer.question.questionText}'),
                                    Text('Twoja odpowiedź: ${answer.answer}'),
                                    Row(
                                      children: [
                                        Text('Poprawna odpowiedź: '),
                                        Text(answer.question.answers.firstWhere((element) => element.isCorrect).content)
                                      ]
                                    )
                                  ]
                              )
                          )
                      ))
                  ).toList() : []
              )
            )
          ],
        ),
      ),
    );
  }
}

