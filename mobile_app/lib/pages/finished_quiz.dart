import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mobile_app/models/arguments.dart';
import 'package:mobile_app/models/models.dart' as models;
import 'package:http/http.dart';
import 'dart:convert';
import 'package:mobile_app/helpers/helpers.dart';

class FinishedQuizView extends StatefulWidget {
  final QuizArguments quizArguments;
  FinishedQuizView(this.quizArguments);

  @override
  _FinishedQuizViewState createState() => _FinishedQuizViewState(quizArguments);
}

class _FinishedQuizViewState extends State<FinishedQuizView> {
  final QuizArguments quizArguments;
  _FinishedQuizViewState(this.quizArguments);

  final storage = new FlutterSecureStorage();
  List<models.QuizResult> quizResults = [];
  models.QuizResult userResult;
  int userRanking = 0;

  @override
  void initState() {
    super.initState();
    fetchResult();
  }

  void fetchResult() async {
    String token = await storage.read(key: 'token');

    Response response = await get(
      '${getApiUrl()}/quizResults/quiz/${quizArguments.quiz.id}',
      headers: <String, String>{
        'Authorization':  'Bearer $token'
      },
    );
    final List data = jsonDecode(response.body);
    List<models.QuizResult> allResults = data.map((val) => models.QuizResult.fromJson(val)).toList();
    dynamic user = JwtDecoder.decode(token);

    setState(() {
      quizResults = allResults;
      userResult = allResults.firstWhere((result) => result.userId == user['id']);
      userRanking = allResults.indexWhere((result) => result.userId == user['id']);
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Quizlab'),
        centerTitle: true,
        backgroundColor: Colors.grey[850],
        elevation: 0.0,
      ),
      backgroundColor: Colors.grey[900],
      body: SafeArea(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Historia quizu ${quizArguments.quiz.name}', style: TextStyle(fontSize: 20, color: Colors.white)),
                SizedBox(height: 20),
                Text('Twój wynik: ${ userResult != null ? '${userResult.points} / ${userResult.maxPoints}' : ''}',
                    style: TextStyle(fontSize: 16, color: Colors.white)
                ),
                SizedBox(height: 10),
                Text('Twój ranking: ${ userRanking + 1} / ${quizResults.length}', style: TextStyle(fontSize: 16, color: Colors.white)),
                SizedBox(height: 50),
                Container(
                  height: 400,
                  child: ListView.builder(
                    itemCount: quizResults.length,
                    itemBuilder: (context, index) {
                      final result = quizResults[index];
                      return ListTile(
                        title: Text(
                            '${index + 1}. ${result.user.username}, ${result.points.toString()} / ${result.maxPoints}',
                            style: TextStyle(color: userResult.userId == result.userId ? Colors.black : Colors.white)
                        ),
                        subtitle: Text(result.user.email, style: TextStyle(color: userResult.userId == result.userId ? Colors.black : Colors.white)),
                        tileColor: userResult.userId == result.userId ? Colors.yellow[400] : Colors.grey[700]
                      );
                    },
                  ),
                )
              ],
            ),
          )
      ),
    );
  }
}