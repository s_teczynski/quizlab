import 'package:flutter/material.dart';

class Constants {
  static Color primaryColor = Color.fromRGBO(232, 138, 18, 1);
  static Color secondaryColor = Color.fromRGBO(23, 97, 160, 1);
}