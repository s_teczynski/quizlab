String getApiUrl() {
  final String DEV_URL = 'http://10.0.2.2:3001';
  final String PRODUCTION_URL = 'http://quizlab-env.eba-ndjpfx7j.us-east-2.elasticbeanstalk.com';
  // final bool isProduction = bool.fromEnvironment('dart.vm.product');
  final bool isProduction = false;
  return isProduction ? PRODUCTION_URL : DEV_URL;
}