import 'package:flutter/material.dart';
import 'package:mobile_app/helpers/constants.dart';
class CustomDrawer extends StatelessWidget {
  CustomDrawer(this.handleLogout) : super();

  final handleLogout;

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
            children: [
              DrawerHeader(child: Image(image: AssetImage('assets/logo.png'))),
              ListTile(
                title: FlatButton.icon(
                  textColor: Constants.secondaryColor,
                  icon: const Icon(Icons.logout),
                  label: Text('Wyloguj'),
                  onPressed: handleLogout
                ),
              ),
            ]
        )
    );
  }
}
