import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile_app/models/models.dart';

class CustomDialog extends StatelessWidget {
  CustomDialog(this.title, this.dropdownNames, this.filters, this.onChangeFilters, this.onFilter);

  String title;
  List<String> dropdownNames;
  AnswersFilters filters;
  final onChangeFilters;
  final onFilter;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: new Text(title),
      content: Column(
        children: [
        DropdownButton<String>(
            value: dropdownNames.contains(filters.quizName) ? filters.quizName : null,
            icon: Icon(Icons.arrow_downward),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: Colors.black),
            underline: Container(
              height: 2,
              color: Colors.yellowAccent,
            ),
            onChanged: (String newValue) {
              onChangeFilters(newValue);
            },
            items: dropdownNames.map((name) => DropdownMenuItem(child: Text(name), value: name)).toList()
          )
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          child: new Text("Zapisz"),
          onPressed: onFilter,
        ),
      ],
    );
  }
}
