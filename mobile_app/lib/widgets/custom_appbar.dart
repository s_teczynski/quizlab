import 'package:flutter/material.dart';
import 'package:mobile_app/helpers/constants.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  CustomAppBar({this.bottom, this.actionIcon, Key key}) : preferredSize = Size.fromHeight(kToolbarHeight), super(key: key);

  @override
  final Size preferredSize; // default is 56.0
  final TabBar bottom;
  final Widget actionIcon;

  @override
  _CustomAppBarState createState() => _CustomAppBarState(this.bottom, this.actionIcon);
}

class _CustomAppBarState extends State<CustomAppBar> {
  _CustomAppBarState(this.bottom, this.actionIcon);

  final TabBar bottom;
  final Widget actionIcon;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text('Quizlab'),
      centerTitle: true,
      backgroundColor: Constants.secondaryColor,
      elevation: 0.0,
      bottom: bottom,
      actions: [
        this.actionIcon != null ? this.actionIcon : SizedBox.shrink()
      ],
    );
  }
}