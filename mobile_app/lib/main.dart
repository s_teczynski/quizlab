import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:mobile_app/pages/home.dart';
import 'package:mobile_app/pages/quizes.dart';
import 'package:mobile_app/pages/quiz.dart';
import 'package:mobile_app/pages/question.dart';
import 'package:mobile_app/pages/login.dart';
import 'package:mobile_app/pages/register.dart';
import 'package:mobile_app/pages/answers.dart';
import 'package:mobile_app/pages/finished_quiz.dart';
import 'package:mobile_app/pages/your_quizes.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mobile_app/helpers/helpers.dart';
import 'package:mobile_app/helpers/constants.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
        var routes = <String, WidgetBuilder>{
          //"/": (ctx) => HomeView(),
          "/main": (ctx) => MyApp(),
          "/login": (ctx) => LoginView(),
          //"/quizes": (ctx) => QuizesCard(),
          "/quiz": (ctx) => QuizView(settings.arguments),
          "/question": (ctx) => QuestionView(settings.arguments),
          "/finished_quiz": (ctx) => FinishedQuizView(settings.arguments)
        };
        WidgetBuilder builder = routes[settings.name];
        return MaterialPageRoute(builder: (ctx) => builder(ctx));
      },
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  final storage = new FlutterSecureStorage();
  int _selectedIndex = 0;
  String _token;
  dynamic _user;
  List<Widget> _authWidgetOptions;
  List<Widget> _notAuthWidgetOptions;
  final key = new GlobalKey<ScaffoldState>();

  @override
  initState() {
    super.initState();
    getTokenFromStorage();
    _authWidgetOptions = <Widget>[
      HomeView(this.handleLogout),
      QuizesCard(this.handleLogout),
      YourQuizesView(this.handleLogout),
      AnswersView(this.handleLogout),
    ];

    _notAuthWidgetOptions = <Widget>[
      LoginView(onLogin: handleLogin),
      RegisterView(onRegister: handleRegister),
    ];
  }

  void getTokenFromStorage() async {
    String token = await storage.read(key: 'token');
    setState(() {
      _token = token;
    });
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void handleLogin(String email, String password) async {
    Map data = {
      'email': email,
      'password': password
    };

    String body = jsonEncode(data);
    dynamic resp = await http.post(
      '${getApiUrl()}/login',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: body,
    );

    if (resp.statusCode != 200) {
      key.currentState.showSnackBar(new SnackBar(
        content: new Text("Wystąpił błąd podczas logowania."),
      ));
      return ;
    }

    String token = resp.body;
    await storage.write(key: 'token', value: token);
    print(token);
    setState(() {
      _token = token;
      _user = JwtDecoder.decode(token);
      _selectedIndex = 0;
    });
  }
  void handleRegister(String email, String username, String password) async {
    Map data = {
      'email': email,
      "username": username,
      'password': password
    };

    String body = jsonEncode(data);

    dynamic resp = await http.post(
      '${getApiUrl()}/register',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: body,
    );

    if (resp.statusCode != 201) {
      key.currentState.showSnackBar(new SnackBar(
        content: new Text("Wystąpił błąd podczas rejestracji"),
      ));
      return ;
    }

    setState(() {
      _selectedIndex = 0;
    });
  }

  void handleLogout() {
    storage.deleteAll();
    Navigator.pop(context);
    setState(() {
      _token = null;
      _selectedIndex = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    BottomNavigationBar navBar = _token == null
      ? (BottomNavigationBar(
        type : BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.login),
            label: 'Logowanie',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Rejestracja',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Constants.primaryColor,
        onTap: _onItemTapped
    ))
    : (BottomNavigationBar(
        type : BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Start',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Quizy',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.edit),
            label: 'Twoje Quizy',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.analytics_outlined),
            label: 'Odpowiedzi',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Constants.primaryColor,
        onTap: _onItemTapped
    ));

    return new Scaffold(
      key: key,
      body: Center(
        child: _token != null ? _authWidgetOptions?.elementAt(_selectedIndex) : _notAuthWidgetOptions?.elementAt(_selectedIndex),
    ),
      bottomNavigationBar: navBar
    );
  }
}